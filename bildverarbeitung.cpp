#include "bildverarbeitung.h"



#include <cstring>



Bildverarbeitung::Bildverarbeitung()
{

    cap.open("/dev/video6");
    // check if we succeeded
    if (!cap.isOpened()) {
        Interne_Kamera_vorhanden = false;
    }
    else{

    }
    aktuelle_Kamera = Webcam;

    //cap2.open("/dev/video5");
    // check if we succeeded
    if (!cap2.isOpened()) {
        externe_Kamera_vorhanden = false;
    }
    else{
    }

    cfg.enable_stream(RS2_STREAM_COLOR, 1920, 1080, RS2_FORMAT_BGR8, 30);
    cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_Z16, 30);
    auto pipe = std::make_shared<rs2::pipeline>();
    p.start(cfg);

}

cv::Mat Bildverarbeitung::Bild_Laden()
{
    cv::Mat mat = cv::imread("Testbild.jpg" );
    cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);

    int breite = Rechteck_Koordinaten[2]-Rechteck_Koordinaten[0];
    int hoehe = Rechteck_Koordinaten[3]-Rechteck_Koordinaten[1];
    int breite_bild = 1280;
    float hoehe_bild = 725;
    int breite_fenster = 640;
    float hoehe_fenster = 480;

    int breite_verhaeltnis = (int)(breite_bild*breite/breite_fenster);
    int hoehe_verhaeltnis = (int)(hoehe_bild*hoehe/hoehe_fenster);

    float breite_verhaeltnis_f = (float)(breite_bild/breite_fenster);
    float hoehe_verhaeltnis_f = (float)(hoehe_bild/hoehe_fenster);

    int breite_original = (int)(breite*breite_verhaeltnis_f);
    int hoehe_original = (int)(hoehe*hoehe_verhaeltnis_f);

    int x_punkt_rechteck = (int)(Rechteck_Koordinaten[0]*breite_verhaeltnis_f);
    int y_punkt_rechteck = (int)(Rechteck_Koordinaten[1]*hoehe_verhaeltnis_f);

    cv::Rect rechteck_gezeichnet(x_punkt_rechteck,y_punkt_rechteck, breite_original, hoehe_original);
    cv::rectangle(mat, rechteck_gezeichnet, cv::Scalar(255,0,0),2 );

    return mat;
}

QImage Bildverarbeitung::Fabraum_Auswahl(cv::Mat mat, int Soll_Farbraum, int HSV_Grenzen[])
{
    cv::Mat channel[3];
    cv::Mat Original;
    cv::Mat HSV_Bild;
    cv::Mat leer = cv::Mat::zeros(mat.rows, mat.cols, CV_8UC1);
    //cv::Mat gemixtes_Bild(mat.rows, mat.cols, CV_8UC3);
    cv::Mat gemixtes_Bild[3];
    Original = mat;

    QImage neues_Bild(mat.size().width, mat.size().height, QImage::Format_Grayscale8);
    QImage neues_Bild_Farbe(mat.size().width, mat.size().height, QImage::Format_RGB888);

    cv::split(mat, channel);

    if(Soll_Farbraum == Farbbild){
        //cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);
        memcpy(neues_Bild_Farbe.scanLine(0), mat.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * mat.channels()));
        neues_Bild = neues_Bild_Farbe;

    }
    else if (Soll_Farbraum == Graubild){

        cv::cvtColor(mat, mat, cv::COLOR_BGR2GRAY);
        memcpy(neues_Bild.scanLine(0), mat.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * mat.channels()));

    }
    else if (Soll_Farbraum == Rotbild){
        //channel[0] = cv::Mat::zeros(mat.rows, mat.cols, CV_8UC1);
        //channel[1] = cv::Mat::zeros(mat.rows, mat.cols, CV_8UC1);
        mat = channel[2];
       // cv::merge(channel,gemixtes_Bild);
        //cv::merge(channel, gemixtes_Bild);
        //gemixtes_Bild = channel;
        //gemixtes_Bild = channel;
        //cv::merge(channel, 3, gemixtes_Bild);
        memcpy(neues_Bild.scanLine(0), mat.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * mat.channels()));
        //memcpy(neues_Bild_Farbe.scanLine(0), gemixtes_Bild.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * gemixtes_Bild.channels()));
        //neues_Bild = neues_Bild_Farbe;

    }
    else if(Soll_Farbraum == Blaubild){

        mat = channel[0];
        memcpy(neues_Bild.scanLine(0), mat.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * mat.channels()));
    }
    else if (Soll_Farbraum == Gruenbild){

        mat = channel[1];
        memcpy(neues_Bild.scanLine(0), mat.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * mat.channels()));

    }
    else if (Soll_Farbraum == HSV){

        cv::cvtColor(mat, mat, cv::COLOR_RGB2HSV);
        cv::inRange(mat, cv::Scalar(HSV_Grenzen[0], HSV_Grenzen[1], HSV_Grenzen[2]), cv::Scalar(HSV_Grenzen[3], HSV_Grenzen[4], HSV_Grenzen[5]), mat);
        memcpy(neues_Bild.scanLine(0), mat.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * mat.channels()));

    }
    else if (Soll_Farbraum == Schwerpunkt){

        cv::cvtColor(mat, mat, cv::COLOR_RGB2HSV);
        cv::inRange(mat, cv::Scalar(HSV_Grenzen[0], HSV_Grenzen[1], HSV_Grenzen[2]), cv::Scalar(HSV_Grenzen[3], HSV_Grenzen[4], HSV_Grenzen[5]), mat);
        HSV_Bild = Schwerpunkt_ermitteln(mat, Original);
        //cv::cvtColor(HSV_Bild, HSV_Bild, cv::COLOR_HSV2RGB);
        memcpy(neues_Bild.scanLine(0), mat.data, static_cast<size_t>(neues_Bild.width() * neues_Bild.height() * mat.channels()));
        //neues_Bild = neues_Bild_Farbe;
    }

    return neues_Bild;

}

QImage Bildverarbeitung::Video_streamen(int Soll_Farbraum, int HSV_Grenzen[], int Kamera_auswahl)
{
    cv::Mat mat_Originalbild;
    cv::Mat mat_Zielbild;
    cv::Mat mat_Tiefenbild;
    cv::Mat channel[3];


    QImage Super_Bild;

    float Abstand_ergebnis = 0.00;

    if(Kamera_auswahl == Keine){
        //Soll alles überspringen oder Bild anzeigen
        Interne_Kamera_vorhanden = false;
        externe_Kamera_vorhanden = false;
    }
    else if(Kamera_auswahl == Webcam){
        cap >> mat_Originalbild;
        Interne_Kamera_vorhanden = true;

    }
    else if (Kamera_auswahl == alte_USB_Kamera) {
        cap2 >> mat_Originalbild;

        externe_Kamera_vorhanden = true;

    }
    else if(Kamera_auswahl == Realsense_Farbbild)
    {
        rs2::frameset frames = p.wait_for_frames();
        rs2::frame color_frame = frames.get_color_frame();
        //frames = p.wait_for_frames();
        //rs2::frame color_frame = frames.get_color_frame();


        cv::Mat realsense(cv::Size(1920, 1080), CV_8UC3, (void*)color_frame.get_data(), cv::Mat::AUTO_STEP);
        mat_Originalbild = realsense;
        externe_Kamera_vorhanden = true;
    }
    else if(Kamera_auswahl == Realsense_Tiefenbild)
    {
        //Erst Farbbild holen
        rs2::frameset frames = p.wait_for_frames();
        rs2::frame color_frame = frames.get_color_frame();
        //frames = p.wait_for_frames();
        //rs2::frame color_frame = frames.get_color_frame();


        cv::Mat realsense(cv::Size(1920, 1080), CV_8UC3, (void*)color_frame.get_data(), cv::Mat::AUTO_STEP);
        mat_Originalbild = realsense;

        // Dann Tiefenbild holen
        //rs2::frameset frames = p.wait_for_frames();
        rs2::depth_frame depth_frame = frames.get_depth_frame();


        cv::Mat realsense_depth(cv::Size(640, 480), CV_16U, (uchar*)depth_frame.get_data(), cv::Mat::AUTO_STEP);
        //cv::Mat realsense_depth(cv::Size(640, 480), CV_8UC1, (uchar*)depth_frame.get_data(), cv::Mat::AUTO_STEP);
        cv::Mat depth8u = realsense_depth;
        depth8u.convertTo(depth8u, CV_8UC1, 255.0/1000);


        //cv::equalizeHist(depth8u, depth8u);
        //cv::applyColorMap(depth8u, depth8u, cv::COLORMAP_JET);

        //Hier soll der Abstand gemessen werden
        if(Maus_Position_an == true){
            std::cout << "Maus : " << Maus_Position[0] << ", "<< Maus_Position[1] << std::endl;
            Abstand_ergebnis = Abstand(depth_frame, Maus_Position[0], Maus_Position[1]);
            std::cout << "Abstand in meter : " << Abstand_ergebnis << std::endl;
        }


        mat_Tiefenbild = depth8u;
        externe_Kamera_vorhanden = true;
    }

    if(Interne_Kamera_vorhanden == true || externe_Kamera_vorhanden == true)
    {
        if(Kamera_auswahl == Webcam || Kamera_auswahl == alte_USB_Kamera)
        {
            cv::flip(mat_Originalbild,mat_Originalbild,1);
        }
        mat_Zielbild = mat_Originalbild;




          //  if(Kamera_auswahl != Realsense_Tiefenbild)
            //{
               cv::cvtColor(mat_Zielbild, mat_Zielbild, cv::COLOR_BGR2RGB);
            //}

            if (Soll_Farbraum == Schwerpunkt){
                cv::drawMarker(mat_Zielbild, cv::Point(Spot[0],Spot[1]),  cv::Scalar(255, 0, 0), cv::MARKER_CROSS, 30, 5, 8);
            }



            if(Kamera_auswahl == Realsense_Tiefenbild)
            {

                QImage Binaerbild(mat_Tiefenbild.size().width, mat_Tiefenbild.size().height, QImage::Format_Grayscale8);
                memcpy(Binaerbild.scanLine(0), mat_Tiefenbild.data, static_cast<size_t>(Binaerbild.width() * Binaerbild.height() * mat_Tiefenbild.channels()));
                Super_Bild = Binaerbild;

                QImage neues_Bild_Farbe2(mat_Zielbild.size().width, mat_Zielbild.size().height, QImage::Format_RGB888);
                memcpy(neues_Bild_Farbe2.scanLine(0), mat_Zielbild.data, static_cast<size_t>(neues_Bild_Farbe2.width() * neues_Bild_Farbe2.height() * mat_Zielbild.channels()));
                Zielbild = neues_Bild_Farbe2;
            }
            else{
                QImage neues_Bild_Farbe2(mat_Zielbild.size().width, mat_Zielbild.size().height, QImage::Format_RGB888);
                memcpy(neues_Bild_Farbe2.scanLine(0), mat_Zielbild.data, static_cast<size_t>(neues_Bild_Farbe2.width() * neues_Bild_Farbe2.height() * mat_Zielbild.channels()));
                Zielbild = neues_Bild_Farbe2;
                Super_Bild = Fabraum_Auswahl(mat_Originalbild, Soll_Farbraum, HSV_Grenzen);
            }

    }
    //Super_Bild = Fabraum_Auswahl(mat_Originalbild, Soll_Farbraum, HSV_Grenzen);

    return Super_Bild;
}


void Bildverarbeitung::Video_streamen_beenden()
{


}

cv::Mat Bildverarbeitung::Schwerpunkt_ermitteln(cv::Mat Bild, cv::Mat Original)
{
    cv::Point Coord;
    cv::Moments mm = cv::moments(Bild,false);
    double moment10 = mm.m10;
    double moment01 = mm.m01;
    double moment00 = mm.m00;
    Coord.x = int(moment10 / moment00);
    Coord.y = int(moment01 / moment00);

    Spot[0] = Coord.x;
    Spot[1] = Coord.y;

    cv::drawMarker(Original, cv::Point(Coord.x,Coord.y),  cv::Scalar(0, 0, 255), cv::MARKER_CROSS, 20, 1, 8);

    return Original;
}

QImage Bildverarbeitung::Originalvideo_anzeigen()
{

    return Zielbild;
}

//Hier soll der Abstand ausgegeben werden, wenn der Benutzer am Bild ein bestimmten Pixel klickt.
// ABstand ist in meter
float Bildverarbeitung::Abstand(rs2::depth_frame depth, int Maus_X, int Maus_Y)
{
       float Abstand = 0.0;

       Abstand = depth.get_distance(130*2, 120*2);




       return Abstand;
}

void Bildverarbeitung::foo_Rechteck_zeichnen(int x, int y, bool erster_Punkt)
{
    if(erster_Punkt == true){
        Rechteck_Koordinaten[0] = x;
        Rechteck_Koordinaten[1] = y;
    }
    else{
        Rechteck_Koordinaten[2] = x;
        Rechteck_Koordinaten[3] = y;
    }
}

Bildverarbeitung::~Bildverarbeitung()
{
    if(cap.isOpened()){
        cap.release();
    }

    if(cap2.isOpened()){
        cap2.release();
    }
    p.stop();

}

