#ifndef BILDVERARBEITUNG_H
#define BILDVERARBEITUNG_H

#include <iostream>
#include <QImage>
#include <opencv4/opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>

#include <librealsense2/rs.hpp>


class Bildverarbeitung
{
public:
    Bildverarbeitung();
    QImage Zielbild;
    int Spot[2] = {0, 0};
    enum Farbraum{Farbbild, Rotbild, Gruenbild, Blaubild, Graubild, HSV, Schwerpunkt};
    int Maus_Position[2] = {0,0};
    int Rechteck_Koordinaten[4] = {0,0,0,0};
    bool Maus_Position_an = false;

    cv::VideoCapture cap;
    cv::VideoCapture cap2;
    cv::VideoCapture cap3;

    rs2::pipeline p;
    rs2::config cfg;

    enum Kameras{Keine, Webcam, alte_USB_Kamera, Realsense_Farbbild, Realsense_Tiefenbild};
    int aktuelle_Kamera = 0;
    bool Interne_Kamera_vorhanden = true;
    bool externe_Kamera_vorhanden = true;


    //QImage neues_Bild;
    cv::Mat Bild_Laden();
    QImage Video_streamen(int Soll_Farbraum, int HSV_Grenzen[], int Kamera_auswahl);
    void Video_streamen_beenden();
    QImage Fabraum_Auswahl(cv::Mat Bild, int Soll_Farbraum, int HSV_Grenzen[]);
    cv::Mat Schwerpunkt_ermitteln(cv::Mat Bild, cv::Mat Original);
    QImage Originalvideo_anzeigen();
    float Abstand(rs2::depth_frame depth, int Maus_X, int Maus_Y);
    void foo_Rechteck_zeichnen(int x, int y, bool erster_punkt);

    ~Bildverarbeitung();

};

#endif // BILDVERARBEITUNG_H
