#ifndef KAMERAEINSTELLUNGEN_H
#define KAMERAEINSTELLUNGEN_H

#include <QDialog>
#include <iostream>

namespace Ui {
class Kameraeinstellungen;
}

class Kameraeinstellungen : public QDialog
{
    Q_OBJECT

public:
    bool gedrueckt = false;
    explicit Kameraeinstellungen(QWidget *parent = 0);
    ~Kameraeinstellungen();

private slots:
    void on_schliessen_clicked();

    void on_drei_clicked();

private:
    Ui::Kameraeinstellungen *ui;
};

#endif // KAMERAEINSTELLUNGEN_H
