#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <bildverarbeitung.h>
#include <about.h>
#include <kameraeinstellungen.h>

#include <iostream>

#include "ui_mainwindow.h"
#include <opencv4/opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <QStandardItemModel>
#include <QFileDialog>
#include <QTextStream>
#include <QFile>
#include <QStringList>
#include <qfile.h>
#include <QMouseEvent>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    Bildverarbeitung Bildverarbeitung_nr1;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool freigabe = true;
    bool Graubild_freigabe = false;
    bool Schwellwert_freigabe = false;
    int Schwellwert_min = 60;
    int Schwellwert_max = 255;
    bool rot_bild_freigabe = false;
    bool gruen_bild_freigabe = false;
    bool blau_bild_freigabe = false;
    bool Range_freigabe = false;
    bool Rechteck_zeichnen = false;
    bool Rechteck_zeichnen_erster_punkt = true;
    int HSV_Grenzen[6] = {0, 0, 0, 255, 255, 255};
    std::string HSV_gespeichert[6] = {"0", "0", "0", "255", "255", "255"};
    int H_Min_Wert = 0;
    int S_Min_Wert = 0;
    int V_Min_Wert = 0;
    int H_Max_Wert = 255;
    int S_Max_Wert = 255;
    int V_Max_Wert = 255;
    enum Farbraum{Farbbild, Rotbild, Gruenbild, Blaubild, Graubild, HSV, Schwerpunkt};
    int Soll_Farbbild = 0;
    QImage aktuelles_Bild;
    QImage Originalbild;
    //QString filename = QFileDialog::getOpenFileName(this, "oeffnen", QDir::currentPath(), "CSV File (*.csv)");
  //  (QFileDialog::getOpenFileName(this, "oeffnen", QDir::currentPath(), "CSV File (*.csv)"));
    std::string aktuelle_Kamera_text = "Keine";
    int aktuelle_Kamera = Bildverarbeitung_nr1.Keine;
    QStringList wordList;
    QFile f;




private slots:
    void on_beenden_clicked();

    void on_Bild_laden_clicked();

    void on_video_clicked();

    void on_Video_beenden_clicked();

    void on_video_pressed();



    void on_checkBox_clicked(bool checked);

    void on_Schwell_Min_Slider_sliderMoved(int position);

    void on_Schwell_Max_Slider_sliderMoved(int position);



    void on_H_Min_sliderMoved(int position);

    void on_S_Min_sliderMoved(int position);

    void on_V_Min_sliderMoved(int position);

    void on_H_Max_sliderMoved(int position);

    void on_S_Max_sliderMoved(int position);

    void on_V_Max_sliderMoved(int position);


    void on_about_knopf_clicked();

    void on_HSV_speichern_clicked();



    void on_Kamera_Liste_box_currentIndexChanged(const QString &arg1);

    void on_HSV_laden_clicked();

    void on_XY_clicked();

    void mousePressEvent(QMouseEvent *event);

    void mouseMoveEvent(QMouseEvent *event);


    void on_actionBeenden_triggered();

    void on_actionKameraeinstellungen_triggered();

    void on_rechteck_pressed();

    void foo_rechteck_zeichen(int x_recht, int y_recht);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
