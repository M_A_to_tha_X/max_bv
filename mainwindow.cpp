#include "mainwindow.h"

//using namespace cv;

/* Anfangswerte werden beim starten des Programmes definiert
 *TODO: man soll vorher aussuchen können welches Bild geladen werden soll
 *ÄNDERUNGEN:
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

   // mModel = new QStandardItemModel(this);
    std::cout << "Bitte löschen" << std::endl;
    setMouseTracking(true);
    Soll_Farbbild = Farbbild;
    aktuelles_Bild = Bildverarbeitung_nr1.Fabraum_Auswahl(Bildverarbeitung_nr1.Bild_Laden() ,Soll_Farbbild, HSV_Grenzen);

    ui->Schwell_Min_Slider->setMinimum(1);
    ui->Schwell_Min_Slider->setMaximum(255);
    ui->Min_Slider_text->setText(QString::number(Schwellwert_min));
    ui->Schwell_Min_Slider->setValue(Schwellwert_min);

    ui->Schwell_Max_Slider->setMinimum(1);
    ui->Schwell_Max_Slider->setMaximum(255);
    ui->Max_Slider_text->setText(QString::number(Schwellwert_max));
    ui->Schwell_Max_Slider->setValue(Schwellwert_max);

    ui->H_Min->setMinimum(1);
    ui->H_Min->setMaximum(255);
    ui->H_Max->setMinimum(1);
    ui->H_Max->setMaximum(255);
    ui->H_Min_Text->setText(QString::number(HSV_Grenzen[0]));
    ui->H_Max_Text->setText(QString::number(HSV_Grenzen[3]));
    ui->H_Min->setValue(HSV_Grenzen[0]);
    ui->H_Max->setValue(HSV_Grenzen[3]);

    ui->S_Min->setMinimum(1);
    ui->S_Min->setMaximum(255);
    ui->S_Max->setMinimum(1);
    ui->S_Max->setMaximum(255);
    ui->S_Min_Text->setText(QString::number(HSV_Grenzen[1]));
    ui->S_Max_Text->setText(QString::number(HSV_Grenzen[4]));
    ui->S_Min->setValue(HSV_Grenzen[1]);
    ui->S_Max->setValue(HSV_Grenzen[4]);

    ui->V_Min->setMinimum(1);
    ui->V_Min->setMaximum(255);
    ui->V_Max->setMinimum(1);
    ui->V_Max->setMaximum(255);
    ui->V_Min_Text->setText(QString::number(HSV_Grenzen[2]));
    ui->V_Max_Text->setText(QString::number(HSV_Grenzen[5]));
    ui->V_Min->setValue(HSV_Grenzen[2]);
    ui->V_Max->setValue(HSV_Grenzen[5]);

    aktuelle_Kamera = Bildverarbeitung_nr1.aktuelle_Kamera;

    switch(aktuelle_Kamera)
    {
    case Bildverarbeitung_nr1.Keine:
        aktuelle_Kamera_text = "Keine";
        break;
    case Bildverarbeitung_nr1.Webcam:
        aktuelle_Kamera_text = "Webcam";
        break;
    case Bildverarbeitung_nr1.alte_USB_Kamera:
        aktuelle_Kamera_text = "alte_USB_Kamera";
        break;
    case Bildverarbeitung_nr1.Realsense_Farbbild:
        aktuelle_Kamera_text = "Kamera_3D";
        break;
    case Bildverarbeitung_nr1.Realsense_Tiefenbild:
        aktuelle_Kamera_text = "Kamera_3D";
        break;
    }

    if(aktuelle_Kamera == Bildverarbeitung_nr1.Keine){

    }

    ui->Kamera_Liste_box->addItem(QString::fromStdString("Keine Webcam"));
    ui->Kamera_Liste_box->addItem(QString::fromStdString("Interne Webcam"));
    ui->Kamera_Liste_box->addItem(QString::fromStdString("alte Webcam"));
    ui->Kamera_Liste_box->addItem(QString::fromStdString("Realsense Farbbild"));
    ui->Kamera_Liste_box->addItem(QString::fromStdString("Realsense Tiefenbild"));

    //std::cout << filename.toStdString() << std::endl;

}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Fenster und Programm werden beendet
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_actionBeenden_triggered()
{
    MainWindow::close();
}

/* Programm wird beendet
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_beenden_clicked()
{
    MainWindow::close();
}

/* Laden eines Bildes.
 * TODO: man soll vorher aussuchen können welches Bild geladen werden soll
 * ÄNDERUNGEN:
 */
void MainWindow::on_Bild_laden_clicked()
{

    freigabe = false;

    if(ui->Graubild->checkState()){
        Soll_Farbbild = Graubild;
    }
    else if (ui->rot_bild->checkState()) {
        Soll_Farbbild = Rotbild;
    }
    else if (ui->blau_bild->checkState()) {
        Soll_Farbbild = Blaubild;
    }
    else if (ui->gruen_bild->checkState()) {
        Soll_Farbbild = Gruenbild;
    }
    else if(ui->Range->checkState()){
        Soll_Farbbild = HSV;
    }
    else if(ui->Schwerpunkt->checkState()){
        Soll_Farbbild = Schwerpunkt;
    }
    else {
        Soll_Farbbild = Farbbild;
    }

    aktuelles_Bild = Bildverarbeitung_nr1.Fabraum_Auswahl(Bildverarbeitung_nr1.Bild_Laden() ,Soll_Farbbild, HSV_Grenzen);

    ui->Bild_Fenster->setPixmap(QPixmap::fromImage(aktuelles_Bild));

    ui->Bild_Fenster->show();
    ui->Bild_Fenster->setScaledContents(true);

}

/* Anzeigen des Live streams. Dabei wird auch der Farbraum angepasst
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_video_clicked()
{

    while(1){

        if(ui->Graubild->checkState()){
            Soll_Farbbild = Graubild;
        }
        else if (ui->rot_bild->checkState()) {
            Soll_Farbbild = Rotbild;
        }
        else if (ui->blau_bild->checkState()) {
            Soll_Farbbild = Blaubild;
        }
        else if (ui->gruen_bild->checkState()) {
            Soll_Farbbild = Gruenbild;
        }
        else if(ui->Range->checkState()){
            Soll_Farbbild = HSV;
        }
        else if(ui->Schwerpunkt->checkState()){
            Soll_Farbbild = Schwerpunkt;
        }
        else {
            Soll_Farbbild = Farbbild;
        }

        aktuelles_Bild = Bildverarbeitung_nr1.Video_streamen( Soll_Farbbild, HSV_Grenzen, aktuelle_Kamera);

        ui->Bild_Fenster->setPixmap(QPixmap::fromImage(aktuelles_Bild));
        qApp->processEvents();
        ui->Bild_Fenster->show();
        ui->Bild_Fenster->setScaledContents(true);

        Originalbild  = Bildverarbeitung_nr1.Originalvideo_anzeigen();

        ui->Original_Fenster->setPixmap(QPixmap::fromImage(Originalbild));
        qApp->processEvents();
        ui->Original_Fenster->show();
        ui->Original_Fenster->setScaledContents(true);


        cv::waitKey(20);

        if(cv::waitKey(30) >=0)break;
        if(freigabe == false)break;

    }
    Bildverarbeitung_nr1.Video_streamen_beenden();
}

/* Live Stream Modus wird beendet
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_Video_beenden_clicked()
{
    freigabe = false;
}

/* Live Stream Modus wird aktiviert. Da soll nur am Anfang wenn geklickt ist passieren. Deswegen "on pressed"
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_video_pressed()
{
    freigabe = true;
}

/* Aktivieren des Schwellwertfilters
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_checkBox_clicked(bool checked)
{
    Schwellwert_freigabe = checked;
}

/* Rauslesen des Wertes des Schwellwert Min Slider
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_Schwell_Min_Slider_sliderMoved(int position)
{
    Schwellwert_min = position;
    ui->Min_Slider_text->setText(QString::number(Schwellwert_min));
}

/* Rauslesen des Wertes des Schwellwert Max Slider
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_Schwell_Max_Slider_sliderMoved(int position)
{
    Schwellwert_max = position;
    ui->Max_Slider_text->setText(QString::number(Schwellwert_max));
}

/* Rauslesen des Wertes des H-Min Slider für HSV Raum
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_H_Min_sliderMoved(int position)
{
    HSV_Grenzen[0] = position;
    ui->H_Min_Text->setText(QString::number(HSV_Grenzen[0]));
}

/* Rauslesen des Wertes des S-Min Slider für HSV Raum
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_S_Min_sliderMoved(int position)
{
    HSV_Grenzen[1] = position;
    ui->S_Min_Text->setText(QString::number(HSV_Grenzen[1]));
}

/* Rauslesen des Wertes des V-Min Slider für HSV Raum
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_V_Min_sliderMoved(int position)
{
    HSV_Grenzen[2] = position;
    ui->V_Min_Text->setText(QString::number(HSV_Grenzen[2]));
}

/* Rauslesen des Wertes des H-Max Slider für HSV Raum
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_H_Max_sliderMoved(int position)
{
    HSV_Grenzen[3] = position;
    ui->H_Max_Text->setText(QString::number(HSV_Grenzen[3]));
}

/* Rauslesen des Wertes des S-Max Slider für HSV Raum
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_S_Max_sliderMoved(int position)
{
    HSV_Grenzen[4] = position;
    ui->S_Max_Text->setText(QString::number(HSV_Grenzen[4]));
}

/* Rauslesen des Wertes des V-Max Slider für HSV Raum
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_V_Max_sliderMoved(int position)
{
    HSV_Grenzen[5] = position;
    ui->V_Max_Text->setText(QString::number(HSV_Grenzen[5]));
}

/* Öffnen des Allgemeine Informationsfenster
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_about_knopf_clicked()
{
    about about1;

    about1.show();
    about1.exec();
}

/* Auswahl an Kameras
 * TODO: Interne und externe Webcam gehen nicht...
 * ÄNDERUNGEN:
 */
void MainWindow::on_Kamera_Liste_box_currentIndexChanged(const QString &arg1)
{
    if(ui->Kamera_Liste_box->currentIndex() == Bildverarbeitung_nr1.Keine){
        aktuelle_Kamera = Bildverarbeitung_nr1.Keine;
    }
    else if(ui->Kamera_Liste_box->currentIndex()== Bildverarbeitung_nr1.Webcam){
        aktuelle_Kamera = Bildverarbeitung_nr1.Webcam;
    }
    else if (ui->Kamera_Liste_box->currentIndex() == Bildverarbeitung_nr1.alte_USB_Kamera) {
        aktuelle_Kamera = Bildverarbeitung_nr1.alte_USB_Kamera;
    }
    else if (ui->Kamera_Liste_box->currentIndex() == Bildverarbeitung_nr1.Realsense_Farbbild) {
    aktuelle_Kamera = Bildverarbeitung_nr1.Realsense_Farbbild;
    }
    else if (ui->Kamera_Liste_box->currentIndex() == Bildverarbeitung_nr1.Realsense_Tiefenbild) {
    aktuelle_Kamera = Bildverarbeitung_nr1.Realsense_Tiefenbild;
    }
}

/* Speichern der HSV-Werte in einer CSV-Datei
 * TODO: noch nicht fertig
 * ÄNDERUNGEN:
 */
void MainWindow::on_HSV_speichern_clicked()
{
    for(int i= 0; i<6; i++)
    {
        HSV_gespeichert[i] = std::to_string(HSV_Grenzen[i]);
    }

    f.setFileName("Datenbank.csv");

    if (f.open(QIODevice::WriteOnly))
    {
        QString data_w = "Hallo";
        QStringData strData;
        //strData.data(HSV_gespeichert);
        //f.setCurrentWriteChannel(1);
        //f.write(QStringData);
        f.write("HAllo23423423");
        f.close();
    }

}

/* Laden der HSV-Werte von einer CSV Datei.
 * TODO: noch nicht fertig
 * ÄNDERUNGEN:
 */
void MainWindow::on_HSV_laden_clicked()
{/*
        QTextStream stream(&filename);
        QString text = stream.readAll();
        QString text2 = stream.readLine();
        std::cout << text2.toStdString() << std::endl;
        */
        //QFile file("Datenbank.csv");

        //QStringList wordList;
        //QByteArray line = file.readLine();
        //wordList.append(line.split(',').first());

        f.setFileName("Datenbank.csv");
        //f();

        if (f.open(QIODevice::ReadOnly))
        {
            QString data;
            data = f.readAll();
            wordList = data.split(',');
            QString wert = wordList.at(0);
            f.close();
            std::cout << "Anzahl Channels: " << f.readChannelCount() << std::endl;
            std::cout << data.toStdString() << std::endl;
           // std::cout << wert.toStdString() << std::endl;
        }
}

/* X und Y Koordinate vom Mausklick werden angzeigt
 * TODO:
 * ÄNDERUNGEN: 1. 20200721, mgr: ein print wurde ausgeklammert
 */
void MainWindow::on_XY_clicked()
{
    //printf("mouse click, %d, %d\n", pos().x(), pos().y());
    std::cout << "mouse click, " << pos().x() << ", " << pos().y() << std::endl;
}

/* Wenn man am linken Fenster mit der Maus klickt, werden die XY Koordinaten gespeichert. Es wird dann die Z Koordinate(Abstand in m) angzeigt(mithilfe von Realsense Kamera).
 * TODO: Koordinatensysetem vom Bild und von der Z-Messung stimmen nicht überein. Siehe REalsense allignment
 * ÄNDERUNGEN:
 */
void MainWindow::mousePressEvent(QMouseEvent *event)
{

    int X = event->pos().x() - 60;
    int Y = event->pos().y() - 80;
    if(X >= 640){
        X = 640;
    }
    if(Y >= 479){
        Y = 479;
    }

   // printf("mouse click, %d, %d\n", event->pos().x(), event->pos().y());
    Bildverarbeitung_nr1.Maus_Position_an = true;
    Bildverarbeitung_nr1.Maus_Position[0] = X;
    Bildverarbeitung_nr1.Maus_Position[1] = Y;
    //std::cout << "mouse click, " << X << ", " << Y << std::endl;

    if(Rechteck_zeichnen == true)
    {
        foo_rechteck_zeichen(X, Y);

    }
}

/* Öffnen des Fensters "Kameraeinstellungen"
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_actionKameraeinstellungen_triggered()
{
    Kameraeinstellungen Kameraeinstellungen_fenster;
    //Kameraeinstellungen_fenster.setModal(true);
    //Kameraeinstellungen_fenster.show();
    //Kameraeinstellungen_fenster.exec();
    if(Kameraeinstellungen_fenster.exec() == QDialog::Accepted)
    {
     //   if(Kameraeinstellungen_fenster.gedrueckt == true)
       // {
            std::cout << "TAste wurde gedrückt2" << std::endl;
        //}
    }

    std::cout << "TAste wurde gedrückt3" << std::endl;
}

/* Zeichnen eines Rechtecks im Fenster
 * Bei betätigen soll ein Rechteck im Fenster gezeichnet werden können
 * TODO:
 * ÄNDERUNGEN:
 */
void MainWindow::on_rechteck_pressed()
{
    Rechteck_zeichnen = true;
    //ui->rechteck->pressed();

}

void MainWindow::foo_rechteck_zeichen(int x, int y)
{
    //printf("mouse click, %d, %d\n", pos().x(), pos().y());
    //std::cout << "mouse click, " << x << ", " << y << std::endl;
    Bildverarbeitung_nr1.foo_Rechteck_zeichnen(x, y, Rechteck_zeichnen_erster_punkt);
    if(Rechteck_zeichnen_erster_punkt == false)
    {
       on_Bild_laden_clicked();
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    int X = event->pos().x() - 60;
    int Y = event->pos().y() - 80;

    std::cout << "mouse bewegung, " << X << ", " << Y << std::endl;
    if(Rechteck_zeichnen == true)
    {
        foo_rechteck_zeichen(X, Y);
        if(Rechteck_zeichnen_erster_punkt == true)
        {
            Rechteck_zeichnen_erster_punkt = false;
        }
    }

}
